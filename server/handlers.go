package server

import (
	"fmt"
	"net/http"
	"os/exec"
	"path/filepath"
	"pitun/ping"
	"time"

	"github.com/gin-gonic/gin"
)

func (s *Server) handleIndex() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "index.html", nil)
	}
}

func (s *Server) handlePing() gin.HandlerFunc {

	type response struct {
		Connected bool `json:"connected"`
		Latency   int  `json:"latency"`
	}

	return func(ctx *gin.Context) {

		chanD := make(chan *ping.Stats, 1)
		go func() {
			res, _ := ping.Ping("1.1.1.1", "eth0", 1, 1*time.Second)
			chanD <- res
		}()
		chanT := make(chan *ping.Stats, 1)
		go func() {
			res, _ := ping.Ping("1.1.1.1", "tun0", 1, 1*time.Second)
			chanT <- res
		}()

		direct := <-chanD
		ds := response{
			Connected: direct.Loss != 100,
			Latency:   direct.Average,
		}

		tunnel := <-chanT
		ts := response{
			Connected: tunnel.Loss != 100,
			Latency:   tunnel.Average,
		}

		ctx.JSON(http.StatusOK, gin.H{"internet": ds, "vpn": ts})
	}
}

func (s *Server) handleTunDown() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		path, err := filepath.Abs("bash/tun-down.sh")
		if err != nil {
			fmt.Println("error:", err)
			ctx.JSON(http.StatusInternalServerError, gin.H{"status": err != nil})
			return
		}

		cmd := exec.Command(path)

		out, err := cmd.CombinedOutput()
		fmt.Println("running", cmd.String())
		fmt.Println(string(out))

		code := http.StatusOK
		if err != nil {
			fmt.Println("error:", err)
			code = http.StatusInternalServerError
		}
		ctx.JSON(code, gin.H{"status": err != nil})
	}
}

func (s *Server) handleTunUp() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		path, err := filepath.Abs("bash/tun-up.sh")
		if err != nil {
			fmt.Println("error:", err)
			ctx.JSON(http.StatusInternalServerError, gin.H{"status": err != nil})
			return
		}

		cmd := exec.Command(path)

		out, err := cmd.CombinedOutput()
		fmt.Println("running", cmd.String())
		fmt.Println(string(out))

		code := http.StatusOK
		if err != nil {
			fmt.Println("error:", err)
			code = http.StatusInternalServerError
		}
		ctx.JSON(code, gin.H{"status": err != nil})
	}
}
