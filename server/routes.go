package server

func (s *Server) routes() {
	s.router.Static("static", "assets/static")
	s.router.LoadHTMLGlob("assets/templates/html/*")

	s.router.GET("/", s.handleIndex())
	s.router.GET("/ping", s.handlePing())
	s.router.GET("/tun/down", s.handleTunDown())
	s.router.GET("/tun/up", s.handleTunUp())
}
