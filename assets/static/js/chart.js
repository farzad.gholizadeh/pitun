Chart.defaults.global.elements.point.radius=0;
Chart.defaults.global.hover.intersect=false;
Chart.defaults.global.hover.mode="index";
Chart.defaults.global.hover.axis="x";

Chart.defaults.global.tooltips.intersect=false;


const ctx = $('#chart');
let chart;
function initChart() {
    chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [],
            datasets: [
                {
                    label: 'Internet',
                    data: [],
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',

                    borderWidth: 1
                },
                {
                    label: 'VPN',
                    data: [],
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                },
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Ping 1.1.1.1'
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            tooltips: {
                mode: 'nearest'
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Latency (ms)'
                    }
                }]
            },
        }
    });
}

initChart();

var myInterval = setInterval(function() {
    requestPing();
}, 1000);

$("#btnDisconnect").click(function() {
    $.ajax({url: "/tun/down"});
});

$("#btnConnect").click(function() {
    $.ajax({url: "/tun/up"});
});

function requestPing(){
    $.ajax({url: "/ping", success: function(result){
            updateChart(result.internet.connected, result.internet.latency, result.vpn.connected, result.vpn.latency)
        }});
}

function updateChart(internetConnected, internetLag, vpnConnected, vpnLag) {
    chart.data.datasets[0].data.push(internetLag);
    chart.data.datasets[1].data.push(vpnLag);

    let today = new Date();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    chart.data.labels.push(time);

    chart.update();

    if(internetConnected) {
        $(".eth-fail").addClass("d-none");
        $(".eth-ok").removeClass("d-none");
    } else {
        $(".eth-ok").addClass("d-none");
        $(".eth-fail").removeClass("d-none");
    }

    if(vpnConnected) {
        $(".tun-fail").addClass("d-none");
        $(".tun-ok").removeClass("d-none");
    } else {
        $(".tun-ok").addClass("d-none");
        $(".tun-fail").removeClass("d-none");
    }
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function removeData(chart) {
    chart.data.datasets[0].data[0] = chart.data.datasets[0].data[1];
    chart.update();
    chart.data.labels.shift();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.shift();
    });
    chart.update();
}