package ovpnqos

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"pitun/ping"
	"sort"
	"strings"
	"sync"
	"time"
)

type ConfFile struct {
	Host     string
	ConfPath string
}

type EffectiveLag struct {
	ConfPath     string
	EffectiveLag int
}

type VPNPingStats struct {
	*ping.Stats
	ConfPath string
}

// ConfFiles returns a channel, containing all files named *.ovpn in rootDir and its subdirectories.
func ConfFiles(rootDir string) <-chan string {
	files := make(chan string, 10)

	go func() {
		_ = filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
			if strings.HasSuffix(path, ".ovpn") {
				files <- path
			}
			return nil
		})

		close(files)
	}()

	return files
}

// ConfToHost gets a channel containing paths to *.ovpn files, and returns a channel containing their remote hosts
func ConfToHost(confFiles <-chan string) <-chan *ConfFile {
	hosts := make(chan *ConfFile, 10)

	go func() {
		for fileName := range confFiles {
			file, err := os.Open(fileName)
			if err != nil {
				log.Printf("failed to open file %v: %s\n", fileName, err)
				continue
			}

			scanner := bufio.NewScanner(file)
			scanner.Split(bufio.ScanLines)

			for scanner.Scan() {
				line := scanner.Text()
				if !strings.HasPrefix(line, "remote ") {
					continue
				}

				params := strings.Split(line, " ")
				if len(params) >= 2 {
					hosts <- &ConfFile{Host: params[1], ConfPath: fileName}
				}

				break
			}

			_ = file.Close()
		}

		close(hosts)
	}()

	return hosts
}

func HostToPingStats(hosts <-chan *ConfFile) <-chan *VPNPingStats {
	timeout := 4 * time.Second
	count := 4
	nic := "eth0"

	out := make(chan *VPNPingStats, 10)
	go func() {
		for host := range hosts {
			stats, err := ping.Ping(host.Host, nic, count, timeout)
			if err != nil {
				_, _ = fmt.Fprintf(os.Stderr, "failed to ping %v\n", host)
				continue
			}
			vpnStats := &VPNPingStats{Stats: stats, ConfPath: host.ConfPath}

			out <- vpnStats
		}

		close(out)
	}()
	return out
}

func PingStatsToEffectiveLag(pingStats <-chan *VPNPingStats) <-chan *EffectiveLag {
	out := make(chan *EffectiveLag, 10)
	go func() {
		for stats := range pingStats {
			if stats.Received == 0 {
				// avoid division by zero
				continue
			}

			lag := stats.Average + stats.Deviation*2
			lag *= stats.Sent / stats.Received // account for packet loss

			out <- &EffectiveLag{
				ConfPath:     stats.ConfPath,
				EffectiveLag: lag,
			}
		}

		close(out)
	}()
	return out
}

func FindBestServers(root string) {
	files := ConfFiles(root)
	hosts := ConfToHost(files)

	var stats []<-chan *VPNPingStats
	for i := 0; i < 200; i++ {
		ps := HostToPingStats(hosts)
		stats = append(stats, ps)
	}
	merged := mergePingStats(stats...)

	scores := PingStatsToEffectiveLag(merged)

	var lags []*EffectiveLag
	for f := range scores {
		lags = append(lags, f)
	}

	sort.Slice(lags, func(i, j int) bool {
		return lags[i].EffectiveLag < lags[j].EffectiveLag
	})

	for _, f := range lags {
		fm, _ := json.Marshal(f)
		fmt.Println(string(fm))
	}
}

func mergePingStats(cs ...<-chan *VPNPingStats) <-chan *VPNPingStats {
	var wg sync.WaitGroup
	out := make(chan *VPNPingStats)

	// Start an output goroutine for each input channel in cs.  output
	// copies values from c to out until c is closed, then calls wg.Done.
	output := func(c <-chan *VPNPingStats) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}
