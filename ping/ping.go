package ping

import (
	"os/exec"
	"regexp"
	"strconv"
	"time"
)

type Stats struct {
	Host string `json:"host"`

	Sent     int `json:"sent"`
	Received int `json:"received"`
	Loss     int `json:"loss"`

	Min       int `json:"min"`
	Average   int `json:"avg"`
	Max       int `json:"max"`
	Deviation int `json:"mdev"`
}

var rx = regexp.MustCompile(
	`(?P<sent>\d+) packets transmitted, (?P<received>\d+) received, (?P<loss>\d+)% packet loss, time \d+ms
rtt min/avg/max/mdev = (?P<min>\d+)\.\d+/(?P<avg>\d+)\.\d+/(?P<max>\d+)\.\d+/(?P<mdev>\d+)\.\d+ ms`)

func Ping(host, nic string, count int, timeout time.Duration) (stats *Stats, err error) {
	countStr := strconv.Itoa(count)
	toStr := strconv.Itoa(int(timeout / time.Second))

	cmd := exec.Command("ping", host, "-I", nic, "-c", countStr, "-w", toStr)
	result, err := cmd.Output()
	if err != nil {
		return &Stats{
			Host:      host,
			Sent:      0,
			Received:  0,
			Loss:      100,
			Min:       0,
			Average:   0,
			Max:       0,
			Deviation: 0,
		}, err
	}

	match := rx.FindStringSubmatch(string(result))
	names := rx.SubexpNames()
	statsMap := make(map[string]string)
	for i, name := range names {
		if i != 0 && name != "" {
			statsMap[name] = match[i]
		}
	}

	stats = &Stats{Host: host}
	stats.Min, _ = strconv.Atoi(statsMap["min"])
	stats.Max, _ = strconv.Atoi(statsMap["max"])
	stats.Average, _ = strconv.Atoi(statsMap["avg"])
	stats.Deviation, _ = strconv.Atoi(statsMap["mdev"])

	stats.Sent, _ = strconv.Atoi(statsMap["sent"])
	stats.Received, _ = strconv.Atoi(statsMap["received"])
	stats.Loss, _ = strconv.Atoi(statsMap["loss"])

	return stats, nil
}
