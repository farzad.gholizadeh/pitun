package cmd

import (
	"pitun/ovpnqos"

	"github.com/spf13/cobra"
)

// findbestCmd represents the findbest command
var findbestCmd = &cobra.Command{
	Use:   "findbest root",
	Short: "Find the best server among the OpenVPN server config files in the root directory",

	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		ovpnqos.FindBestServers(args[0])
	},
}

func init() {
	rootCmd.AddCommand(findbestCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// findbestCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// findbestCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
